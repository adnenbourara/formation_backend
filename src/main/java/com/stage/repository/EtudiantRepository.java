package com.stage.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.stage.model.entity.Etudiant;

@Repository
public interface EtudiantRepository extends CrudRepository<Etudiant, Long>  {
	List<Etudiant> findByClasseId(Long id);
}
