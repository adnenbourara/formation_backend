package com.stage.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.stage.model.entity.Classe;


@Repository
public interface ClasseRepository extends CrudRepository<Classe,Long> {
	
}
