package com.stage.rest.controller;

import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.stage.model.entity.Classe;

import com.stage.rest.dto.ClasseDto;

import com.stage.service.ClasseService;

@RestController
@CrossOrigin
public class ClasseController {
	
	@Autowired
	private ClasseService classeService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	
	@GetMapping("/test")
	public String testController()
	{
		return "hello adnen";
	}
	
	@PostMapping("/classes")
	public Object addClasse(@RequestBody ClasseDto classeDto)
	{
		Classe classe = modelMapper.map(classeDto, Classe.class);
		classe = classeService.addClasse(classe);
		classeDto = modelMapper.map(classe, ClasseDto.class);
		return ResponseEntity.status(HttpStatus.CREATED).body(classeDto);
		
	}
	
 @GetMapping("/classes/{id}")
 public Object getById(@PathVariable Long id)
 {
	 	Classe classe= classeService.getById(id);
	 	ClasseDto classeDto=modelMapper.map(classe, ClasseDto.class);
		 return ResponseEntity.status(HttpStatus.OK).body(classeDto);
 }
 
 @PutMapping("/classes/{id}")
 public Object editClasse(@RequestBody ClasseDto classeDto,@PathVariable Long id)
 {
	 Classe classe=modelMapper.map(classeDto , Classe.class);
     classe = classeService.editClasse(classe, id);
     classeDto = modelMapper.map(classe, ClasseDto.class);
	return ResponseEntity.status(HttpStatus.CREATED).body(classeDto);
}
 
 @DeleteMapping("/classes/{id}")
 public Object deleteClasse(@PathVariable Long id)
 {
	classeService.deleteClasse(id);
   	return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
	}
 
 
 @GetMapping("/classes")
 public Object getAllClasse()
 {
	 List<Classe> classe = classeService.getAllClasse();
 Type listType = new TypeToken <List<ClasseDto>>() {}.getType();
 List <ClasseDto> classeDto= modelMapper.map(classe,listType);
 
 
	 return ResponseEntity.status(HttpStatus.OK).body(classeDto);
 }
 

 }
 
 
	



