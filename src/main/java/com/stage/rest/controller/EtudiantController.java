package com.stage.rest.controller;

import java.lang.reflect.Type;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.stage.model.entity.Classe;
import com.stage.model.entity.Etudiant;
import com.stage.rest.dto.EtudiantDto;
import com.stage.service.ClasseService;
import com.stage.service.EtudiantService;

@RestController
@CrossOrigin
public class EtudiantController {

	@Autowired
	private EtudiantService etudiantService;
	@Autowired
	private ClasseService classeService;

	@Autowired
	private ModelMapper modelMapper;


	
	@PostMapping("/classes/{id}/etudiants")
	public Object addEtudiant(@RequestBody EtudiantDto etudiantDto, @PathVariable Long id) {
		Etudiant etudiant = modelMapper.map(etudiantDto, Etudiant.class);
		Classe classe= classeService.getById(id);
		etudiant.setClasse(classe);
		etudiant = etudiantService.addEtudiant(etudiant);
		etudiantDto = modelMapper.map(etudiant, EtudiantDto.class);
return ResponseEntity.status(HttpStatus.CREATED).body(etudiantDto);

	}
	
	
     @GetMapping("/etudiants/{id}")
     public Object getById(@PathVariable Long id)
{
     Etudiant etudiant = etudiantService.getById(id);
     EtudiantDto etudiantDto = modelMapper.map(etudiant , EtudiantDto.class);
	 return ResponseEntity.status(HttpStatus.OK).body(etudiantDto);
}
	
  @PutMapping("/etudiants/{id}")
public Object editEtudiant(@RequestBody EtudiantDto etudiantDto ,@PathVariable Long id)
 {    Etudiant etudiant = modelMapper.map(etudiantDto,Etudiant.class);
	  etudiant = etudiantService.editEtudiant(id,etudiant);
      etudiantDto = modelMapper.map(etudiant, EtudiantDto.class);
	 
      return ResponseEntity.status(HttpStatus.CREATED).body(etudiantDto);
	  }
  
  @DeleteMapping("/etudiants/{id}")
  public Object deleteEtudiant(@PathVariable Long id) {
	  etudiantService.deleteEtudiant(id);
	  return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);  
  }
  
  @GetMapping("/etudiants")
  public Object getAllEtudiant() 
  { List <Etudiant> etudiant= etudiantService.getAllEtudiant();
  Type listType = new TypeToken <List<EtudiantDto>>() {} .getType();
  List <EtudiantDto> etudiantDto= modelMapper.map(etudiant, listType);
   return ResponseEntity.status(HttpStatus.OK).body(etudiantDto);
}
  @GetMapping("classes/{id}/etudiants")
  public Object getAllEtudiantFromClasse(@PathVariable Long id) 
  {  
  List <Etudiant> etudiant= etudiantService.getAllEtudiantFromClasse(id);
  Type listType = new TypeToken <List<EtudiantDto>>() {} .getType();
  List <EtudiantDto> etudiantDto= modelMapper.map(etudiant, listType);
   return ResponseEntity.status(HttpStatus.OK).body(etudiantDto);
}
}