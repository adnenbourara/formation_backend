package com.stage.rest.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude 

public class EtudiantDto {

private Long id;
private String nom;
private String prenom;
private String email;
private Date date;
private ClasseDto classe;

}
