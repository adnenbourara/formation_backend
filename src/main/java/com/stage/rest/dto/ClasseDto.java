package com.stage.rest.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonInclude
public class ClasseDto {
	
	private Long id;
	private String label;
	private String fullname;

}
