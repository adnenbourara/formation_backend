package com.stage.model.entity;



import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor

public class Classe {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "LABEL", length = 50)
	private String label;
	
	@Column(name = "FULLNAME", length = 50)
	private String fullname;
	
	@OneToMany (mappedBy= "classe",  cascade = CascadeType.ALL )
	private  List<Etudiant>  ListEtudiants;
	


}
