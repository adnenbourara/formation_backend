package com.stage.model.entity;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Etudiant {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;
	
	@Column(name ="NAME", length = 50)
	private String nom;
	
	
	@Column(name = "FAMILY_NAME", length = 50)
	private String prenom;
	
	
	@Column(name = "EMAIL", length = 50)
	private String email;
	
	@Column(name = "DATE")
	@Temporal(value=TemporalType.DATE)
	private Date date;
	
	@ManyToOne
	@JoinColumn(name ="CLASSE")
	private Classe classe;
	
	
	
	
	
	
	

}
