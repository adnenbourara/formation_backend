package com.stage.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stage.model.entity.Classe;


import com.stage.repository.ClasseRepository;
import com.stage.service.ClasseService;

@Service
public class ClasseServiceImpl implements ClasseService{
	
	@Autowired
	private ClasseRepository classeRepository;
	
	
	@Override
	public Classe addClasse(Classe classe) {
		return classeRepository.save(classe) ;
	}

	@Override
	public Classe getById(Long id) {
		 return classeRepository.findById(id).get();
	}

	@Override
	public Classe editClasse(Classe classe,Long id) {
		getById(id);
		classe.setId(id);
		return classeRepository.save(classe);
	}

	@Override
	public void deleteClasse(Long id) {
		 classeRepository.deleteById(id);
			
	}

	@Override
	public List<Classe> getAllClasse() {
		return(List<Classe> )classeRepository.findAll();
	}
	


}
