package com.stage.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stage.model.entity.Etudiant;
import com.stage.repository.EtudiantRepository;
import com.stage.service.EtudiantService;


@Service
public class EtudiantServiceImpl implements EtudiantService {
 
	
	
	@Autowired
	private EtudiantRepository etudiantRepository;
	
	@Override
	public Etudiant addEtudiant(Etudiant etudiant) {
		return etudiantRepository.save(etudiant);
		
	}
	
	

	@Override
	public Etudiant editEtudiant(Long id ,Etudiant etudiant) {
    getById(id);
	etudiant.setId(id);
	return etudiantRepository.save(etudiant); 
	}



	@Override
	public Etudiant getById(Long id) {
 return etudiantRepository.findById(id).get();
	}
	
	@Override
	public void deleteEtudiant(Long id) 
	{ etudiantRepository.deleteById(id);
		
	}



	@Override
	public List<Etudiant> getAllEtudiant() {
		return(List<Etudiant> )etudiantRepository.findAll();
	}
	
	@Override
	public List<Etudiant> getAllEtudiantFromClasse(Long id)
	{
		return(List<Etudiant>)etudiantRepository.findByClasseId(id);
	}
}
