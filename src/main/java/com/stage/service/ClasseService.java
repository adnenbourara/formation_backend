package com.stage.service;

import java.util.List;

import com.stage.model.entity.Classe;



public interface ClasseService {
	

	 Classe addClasse(Classe classe);
	 Classe getById(Long id);
	 Classe editClasse(Classe classe ,Long id);
	 void deleteClasse(Long id);
	 List<Classe> getAllClasse();
	 


}
