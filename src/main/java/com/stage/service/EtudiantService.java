package com.stage.service;

import java.util.List;

import com.stage.model.entity.Etudiant;


public interface EtudiantService {

	 Etudiant addEtudiant(Etudiant etudiant);
	 Etudiant getById(Long id);
	 Etudiant editEtudiant(Long id ,Etudiant etudiant);
	 void deleteEtudiant(Long id);
	 List<Etudiant> getAllEtudiant();
	 List<Etudiant> getAllEtudiantFromClasse(Long id);
	

}
